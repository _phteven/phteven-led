#ifndef PHTEVEN_LED_TYOES_H
#define PHTEVEN_LED_TYOES_H

struct Segment {
    uint8_t start;
    uint8_t end;
    CRGB color;
};

struct ColorState {
    uint8_t brightness;
    uint8_t numOfSegments;
    unsigned long version;
};

struct WifiNetwork {
    String ssid;
    String rssi;
    bool secure;
};

#endif //PHTEVEN_LED_TYOES_H
