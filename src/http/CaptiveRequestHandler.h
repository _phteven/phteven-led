#ifndef PHTEVEN_LED_CAPTIVEREQUESTHANDLER_H
#define PHTEVEN_LED_CAPTIVEREQUESTHANDLER_H

#include <ESPAsyncWebServer.h>
#include <utility>
#include <LittleFS.h>

typedef std::function<void(const char *ssid, const char *password)> WifiCredentialsSubmitCallback;

class CaptiveRequestHandler : public AsyncCallbackWebHandler {

private:
    WifiCredentialsSubmitCallback _submitCallback;

    void handleGet(AsyncWebServerRequest *request) const {
        request->send(LittleFS, "/index.html", "text/html", false);
    }

    void handlePost(AsyncWebServerRequest *request) {
        if (!request->hasParam("ssid", true)) {
            request->redirect("/");

            return;
        }

        if (!request->hasParam("password", true)) {
            request->redirect("/");

            return;
        }

        auto response = request->beginResponseStream("text/html");
        response->print("Connecting...");

        auto ssid = request->getParam("ssid", true)->value();
        auto password = request->getParam("password", true)->value();

        _submitCallback(ssid.c_str(), password.c_str());

        request->send(response);
    }

public:
    explicit CaptiveRequestHandler(WifiCredentialsSubmitCallback fn) : _submitCallback(std::move(fn)) {
        onRequest([this](AsyncWebServerRequest *request) {
            if (request->method() == HTTP_POST) {
                this->handlePost(request);
            }

            if (request->method() == HTTP_GET) {
                this->handleGet(request);
            }
        });
    }
};

#endif //PHTEVEN_LED_CAPTIVEREQUESTHANDLER_H
