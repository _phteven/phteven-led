#ifndef LED_CONTROLLER_SEQUENCE_H
#define LED_CONTROLLER_SEQUENCE_H

#include "FastLEDWriter.h"

class Sequence {

public:
    explicit Sequence(FastLEDWriter *ledWriter) : _ledWriter(ledWriter) {}

    virtual void run(unsigned int delta) = 0;

protected:
    FastLEDWriter *_ledWriter;
};

#endif //LED_CONTROLLER_SEQUENCE_H
