#include "FastLEDWriter.h"

const auto POWER_LED_OFFSET = 0;

uint8_t pinD0 = 16;
uint8_t pinD1 = 5;
uint8_t pinD2 = 4;
uint8_t pinD3 = 0;
uint8_t pinD4 = 2;

FastLEDWriter::FastLEDWriter() {
    FastLED.addLeds<NEOPIXEL, 16>(_leds, 0, NUM_LEDS);
    FastLED.addLeds<NEOPIXEL, 5>(_leds, 0, NUM_LEDS);
}

void FastLEDWriter::show() {
    if (POWER_LED_OFFSET > 0) {
        _leds[0] = Black;
    }
    FastLED.show();
}

void FastLEDWriter::setBrightness(uint8_t brightness) {
    FastLED.setBrightness(brightness);
}

void FastLEDWriter::fillAll(CRGB color) {
    for (int i = POWER_LED_OFFSET; i < NUM_LEDS; i++) {
        _leds[i] = color;
    }
}

const uint8_t FastLEDWriter::getBrightness() {
    return FastLED.getBrightness();
}

void FastLEDWriter::fill(int startIndex, int endIndex, CRGB color) {
    for (int i = startIndex + POWER_LED_OFFSET; i < endIndex + POWER_LED_OFFSET; i++) {
        _leds[i] = color;
    }
}

int FastLEDWriter::getNumLed() {
    return NUM_LEDS - POWER_LED_OFFSET;
}
