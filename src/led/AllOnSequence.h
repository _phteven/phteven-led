#ifndef LED_CONTROLLER_ALLONSEQUENCE_H
#define LED_CONTROLLER_ALLONSEQUENCE_H

#include "Sequence.h"

class AllOnSequence : public Sequence {

public:
    explicit AllOnSequence(FastLEDWriter *ledWriter) : Sequence(ledWriter) {};

    void run(unsigned int delta) override {
    }
};


#endif //LED_CONTROLLER_ALLONSEQUENCE_H
