#ifndef LED_CONTROLLER_LEDWRITER_H
#define LED_CONTROLLER_LEDWRITER_H

#include "FastLED.h"

#define NUM_LEDS 115

const auto White = CRGB{255, 255, 255};
const auto Black = CRGB{0, 0, 0};

class FastLEDWriter {

public:
    explicit FastLEDWriter();

    static void setBrightness(uint8_t brightness);

    void show();

    void fillAll(CRGB color);

    static const uint8_t getBrightness();

    void fill(int startIndex, int endIndex, CRGB color);

    static int getNumLed();

private:
    CRGB _leds[NUM_LEDS];
};


#endif //LED_CONTROLLER_LEDWRITER_H
