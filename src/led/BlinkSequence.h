#ifndef PHTEVEN_LED_BLINKSEQUENCE_H
#define PHTEVEN_LED_BLINKSEQUENCE_H

#include <CubeArduino.h>
#include "Sequence.h"
#include "FastLEDWriter.h"

class BlinkSequence : public Sequence {

public:
    explicit BlinkSequence(FastLEDWriter *ledWriter) : Sequence(ledWriter) {};

    void run(unsigned int delta) override {
        auto color = this->white ? White : Black;
        _ledWriter->fillAll(color);

        _blinkInterval->update(delta);
    }

private:
    bool white = true;

    Interval *_blinkInterval = new BasicInterval(1500, [this] {
        this->white = !this->white;
    });
};

#endif //PHTEVEN_LED_BLINKSEQUENCE_H
