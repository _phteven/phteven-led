#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>
#include <ESP8266mDNS.h>
#include <CubeArduino.h>
#include <led/FastLEDWriter.h>
#include <led/AllOnSequence.h>
#include <led/BlinkSequence.h>
#include <ESP_EEPROM.h>
#include <LittleFS.h>
#include "http/CaptiveRequestHandler.h"
#include "AsyncJson.h"
#include "ArduinoJson.h"
#include "./types.h"
#include "./constants.h"

auto apName = "phteven-led-" + String(system_get_chip_id());
auto apPassword = "phteven-led";

void registerMDNS() {
    if (MDNS.begin(apName, WiFi.localIP())) {
        MDNS.addService("phteven-led", "tcp", 80);
    }
}

bool connectWifi(const String &ssid, const String &password, bool &isConnecting, unsigned long &lastConnectTry) {
    if (WiFi.status() == WL_CONNECTED) {
        return false;
    }

    if (ssid == "") {
        return false;
    }

    isConnecting = true;
    lastConnectTry = millis();

    WiFi.mode(WIFI_STA);

    ETS_UART_INTR_DISABLE();
    wifi_station_disconnect();
    ETS_UART_INTR_ENABLE();

    if (ssid == WiFi.SSID()) {
        WiFi.begin();
    } else {
        WiFi.begin(ssid, password);
    }

    Serial.println("Wifi begin");

    return true;
}

void setupAp() {
    Serial.println("setupAp");

    WiFi.disconnect();
    WiFi.mode(WIFI_AP);
    delay(200);
    WiFi.softAP(apName, apPassword);
}

const auto INIT_BYTE_VALUE = 123;

void setup() {
    delay(200);

    Serial.begin(BAUD);
    delay(200);

    LittleFS.begin();
    delay(200);

    EEPROM.begin(EEPROM_SIZE);
    delay(200);

    Platform *_platform = new ArduinoPlatform();
    auto _delta = new Delta(_platform);
    auto _ledWriter = new FastLEDWriter();
    auto _setupLedSequence = new BlinkSequence(_ledWriter);
    auto _defaultLedSequence = new AllOnSequence(_ledWriter);
    auto _updateLedInterval = new BasicInterval(LED_UPDATE_INTERVAL, [&_ledWriter] {
        _ledWriter->show();
    });

    Segment _segments[MAX_SEGMENTS];
    ColorState _colorState = {255, 1, 0};

    uint8_t initByte = 0;
    EEPROM.get(INIT_BYTE_ADDRESS, initByte);
    if (initByte != INIT_BYTE_VALUE) {
        EEPROM.put(NUM_SEGMENTS_EEPROM_ADDRESS, _colorState.numOfSegments);
        EEPROM.put(BRIGHTNESS_EEPROM_ADDRESS, _colorState.brightness);
        EEPROM.put(SEGMENTS_START_EEPROM_ADDRESS, 255);
        EEPROM.put(SEGMENTS_START_EEPROM_ADDRESS + 1, 255);
        EEPROM.put(SEGMENTS_START_EEPROM_ADDRESS + 2, 255);
        EEPROM.put(SEGMENTS_START_EEPROM_ADDRESS + 3, 0); // start
        EEPROM.put(SEGMENTS_START_EEPROM_ADDRESS + 4, FastLEDWriter::getNumLed()); // end
        EEPROM.put(INIT_BYTE_ADDRESS, INIT_BYTE_VALUE);
        EEPROM.commit();
    }

    EEPROM.get(NUM_SEGMENTS_EEPROM_ADDRESS, _colorState.numOfSegments);
    EEPROM.get(BRIGHTNESS_EEPROM_ADDRESS, _colorState.brightness);

    if (_colorState.numOfSegments > MAX_SEGMENTS || _colorState.numOfSegments == 0) {
        _colorState.numOfSegments = 1;
        _segments[0] = {0, static_cast<uint8_t>(FastLEDWriter::getNumLed()), CRGB{255, 255, 255}};
    }

    if (_colorState.brightness > 255 || _colorState.brightness < 0) {
        _colorState.brightness = 255;
    }

    for (int i = 0; i < _colorState.numOfSegments; i++) {
        Segment segment = {0, static_cast<uint8_t>(FastLEDWriter::getNumLed()), CRGB{255, 255, 255}};

        auto address = (i * 5) + SEGMENTS_START_EEPROM_ADDRESS;
        EEPROM.get(address, segment.color.r);
        EEPROM.get(address + 1, segment.color.g);
        EEPROM.get(address + 2, segment.color.b);
        EEPROM.get(address + 3, segment.start);
        EEPROM.get(address + 4, segment.end);

        if (segment.start > FastLEDWriter::getNumLed()) {
            segment.start = 0;
        }

        if (segment.end > FastLEDWriter::getNumLed()) {
            segment.end = FastLEDWriter::getNumLed();
        }

        _segments[i] = segment;
    }

    WiFi.mode(WIFI_STA);
    WiFi.setAutoConnect(true);
    WiFi.persistent(true);
    WiFi.hostname(apName);
    auto resetIp = IPAddress(0, 0, 0, 0);
    WiFi.config(resetIp, resetIp, resetIp);

    String _ssid = WiFi.SSID();
    String _password = "";
    bool _reset = false;
    int _wifiScanId = 0;
    auto _isConnecting = false;
    unsigned long _lastConnectTry = 0;
    auto _wifiStatus = WiFi.status();

    auto _connectInterval = new BasicInterval(
            CONNECT_WIFI_INTERVAL,
            [&_ssid, &_password, &_wifiStatus, &_isConnecting, &_lastConnectTry] {
                if (_isConnecting || _wifiStatus == WL_CONNECTED) {
                    return;
                }

                connectWifi(_ssid, _password, _isConnecting, _lastConnectTry);
            }
    );

    bool firstConnecting = false;
    if (_ssid != "") {
        Serial.println("Using previously saved SSID: " + String(_ssid));

        firstConnecting = connectWifi(_ssid, "", _isConnecting, _lastConnectTry);
    }

    if (!firstConnecting) {
        _ssid = "";
        setupAp();
    }

    uint8_t _lastWifiScanCount = 0;
    auto _lastWifiScan = 0;
    WifiNetwork *_lastWifiNetworks;

    AsyncWebServer server(80);

    for (auto staticFile: {"/jquery.min.js"}) {
        server.serveStatic(staticFile, LittleFS, staticFile)
                .setCacheControl("max-age=3600")
                .setFilter(ON_AP_FILTER);
    }

    server.on("/scan", HTTP_GET,
              [&_wifiScanId, &_lastWifiScan, &_lastWifiScanCount, &_lastWifiNetworks](AsyncWebServerRequest *request) {
                  int8_t n = WiFi.scanComplete();
                  auto canScanAgain = _lastWifiScan + (7 * 1000) < millis();

                  if (n > 0) {
                      auto wifiNetworks = new WifiNetwork[n];

                      for (int i = 0; i < n; ++i) {
                          wifiNetworks[i] = {WiFi.SSID(i), String(WiFi.RSSI(i)), WiFi.encryptionType(i) > 0};
                      }

                      _lastWifiScanCount = n;
                      _lastWifiNetworks = wifiNetworks;
                  }

                  if (canScanAgain) {
                      WiFi.scanDelete();
                      WiFi.scanNetworks(true);
                      _lastWifiScan = millis();
                      _wifiScanId += 1;
                  }

                  auto response = new AsyncJsonResponse();
                  auto root = response->getRoot();
                  root["scan_id"] = _wifiScanId;
                  root.createNestedArray("networks");

                  for (int i = 0; i < _lastWifiScanCount; ++i) {
                      auto networkItem = _lastWifiNetworks[i];
                      auto network = root["networks"].as<JsonArray>().createNestedObject();
                      network["rssi"] = networkItem.rssi != nullptr ? networkItem.rssi : "";
                      network["ssid"] = networkItem.ssid != nullptr ? networkItem.ssid : "";
                      network["secure"] = networkItem.secure;
                  }

                  response->setLength();
                  request->send(response);
              }).setFilter(ON_AP_FILTER);

    server.addHandler(new CaptiveRequestHandler([&_ssid, &_password](const char *ssid, const char *password) {
        _ssid = ssid;
        _password = password;
    })).setFilter(ON_AP_FILTER);

    server.on("/info", HTTP_GET, [](AsyncWebServerRequest *request) {
        auto *response = new AsyncJsonResponse();
        auto root = response->getRoot();
        root["id"] = apName;
        root["ssid"] = WiFi.SSID();
        root["mode"] = WiFi.getMode();
        root["mac"] = WiFi.macAddress();

        response->setLength();
        request->send(response);
    }).setFilter(ON_STA_FILTER);

    auto *putColorHandler = new AsyncCallbackWebHandler();
    putColorHandler->setUri("/color");
    putColorHandler->setMethod(HTTP_PUT);
    putColorHandler->onRequest([&_colorState, &_segments](AsyncWebServerRequest *request) {
        DynamicJsonDocument jsonBuffer(1024);
        DeserializationError error = deserializeJson(jsonBuffer, (uint8_t *) (request->_tempObject));
        auto *response = new AsyncJsonResponse();
        auto root = response->getRoot();

        if (error) {
            root["error"] = error.code();

            response->setCode(500);
            response->setLength();
            request->send(response);

            return;
        }

        JsonVariant json = jsonBuffer.as<JsonVariant>();

        auto body = json.as<JsonObject>();
        auto version = body["version"].as<unsigned long>();

        if (version < _colorState.version) {
            root["version"] = _colorState.version;

            response->setCode(200);
            response->setLength();
            request->send(response);

            return;
        }

        auto bodySegments = body["segments"].as<JsonArray>();
        uint8_t numSegments = bodySegments.size();

        if (numSegments == 0) {
            root["error"] = "Must specify at least 1 segment";

            response->setCode(400);
            response->setLength();
            request->send(response);

            return;
        }

        if (numSegments > MAX_SEGMENTS) {
            root["error"] = "Too many segments.";

            response->setCode(400);
            response->setLength();
            request->send(response);

            return;
        }

        for (int i = 0; i < numSegments; i++) {
            auto colorSegment = bodySegments.getElement(i).as<JsonObject>();

            uint8_t red = colorSegment["red"].as<uint8_t>();
            uint8_t green = colorSegment["green"].as<uint8_t>();
            uint8_t blue = colorSegment["blue"].as<uint8_t>();
            uint8_t startIndex = Platform::Map(colorSegment["start"].as<uint8_t>(), MIN_SEGMENT_OFFSET,
                                               MAX_SEGMENT_OFFSET, 0, FastLEDWriter::getNumLed());
            uint8_t endIndex = Platform::Map(colorSegment["end"].as<uint8_t>(), MIN_SEGMENT_OFFSET, MAX_SEGMENT_OFFSET,
                                             0, FastLEDWriter::getNumLed());

            auto color = CRGB{red, green, blue};

            _segments[i] = {startIndex, endIndex, color};
        }

        uint8_t brightness = body["brightness"].as<uint8_t>();

        _colorState = {brightness, numSegments, version};

        root["version"] = _colorState.version;
        response->setCode(200);
        response->setLength();
        request->send(response);
    });
    putColorHandler->onBody([](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
        if (total > 0 && request->_tempObject == nullptr) {
            request->_tempObject = malloc(total);
        }

        if (request->_tempObject != nullptr) {
            memcpy((uint8_t *) (request->_tempObject) + index, data, len);
        }
    });
    putColorHandler->setFilter(ON_STA_FILTER);

    server.addHandler(putColorHandler);

    server.on("/color", HTTP_GET, [&_colorState, &_segments](AsyncWebServerRequest *request) {
        auto *response = new AsyncJsonResponse();
        auto root = response->getRoot();
        root["brightness"] = _colorState.brightness;
        root["version"] = _colorState.version;
        root.createNestedArray("segments");

        for (int i = 0; i < _colorState.numOfSegments; i++) {
            auto segment = _segments[i];
            auto jsonSegment = root["segments"].as<JsonArray>().createNestedObject();

            jsonSegment["red"] = segment.color.r;
            jsonSegment["green"] = segment.color.g;
            jsonSegment["blue"] = segment.color.b;
            jsonSegment["start"] = Platform::Map(segment.start, 0, FastLEDWriter::getNumLed(), MIN_SEGMENT_OFFSET,
                                                 MAX_SEGMENT_OFFSET);
            jsonSegment["end"] = Platform::Map(segment.end, 0, FastLEDWriter::getNumLed(), MIN_SEGMENT_OFFSET,
                                               MAX_SEGMENT_OFFSET);
        }

        response->setLength();
        request->send(response);
    }).setFilter(ON_STA_FILTER);

    server.on("/save", HTTP_PUT, [&_colorState, &_segments](AsyncWebServerRequest *request) {
        EEPROM.put(NUM_SEGMENTS_EEPROM_ADDRESS, _colorState.numOfSegments);
        EEPROM.put(BRIGHTNESS_EEPROM_ADDRESS, _colorState.brightness);

        for (int i = 0; i < _colorState.numOfSegments; i++) {
            auto segment = _segments[i];

            auto address = (i * 5) + SEGMENTS_START_EEPROM_ADDRESS;

            EEPROM.put(address, segment.color.r);
            EEPROM.put(address + 1, segment.color.g);
            EEPROM.put(address + 2, segment.color.b);
            EEPROM.put(address + 3, segment.start);
            EEPROM.put(address + 4, segment.end);
        }

        auto result = EEPROM.commit();
        request->send(result ? 200 : 500);
    }).setFilter(ON_STA_FILTER);

    server.on("/reset", HTTP_PUT, [&_reset](AsyncWebServerRequest *request) {
        request->send(200);
        _reset = true;
    }).setFilter(ON_STA_FILTER);

    server.begin();

    DNSServer dnsServer;
    dnsServer.start(DNS_PORT, "*", WiFi.softAPIP());
    Serial.println(WiFi.softAPIP().toString());

    while (true) {
        auto delta = _delta->update();

        _updateLedInterval->update(delta);
        _connectInterval->update(delta);

        if (_reset) {
            setupAp();
            _ssid = "";
            _password = "";
            _reset = false;
        }

        auto currentWifiStatus = WiFi.status();

        if (currentWifiStatus == WL_CONNECTED && _wifiStatus != WL_CONNECTED) {
            Serial.println("Wifi did connect");

            _isConnecting = false;
            registerMDNS();
        }

        if (currentWifiStatus != WL_CONNECTED && _wifiStatus == WL_CONNECTED) {
            Serial.println("Wifi did disconnect");

            setupAp();
            MDNS.end();
        }

        if (_wifiStatus == WL_DISCONNECTED) {
            dnsServer.processNextRequest();
            _setupLedSequence->run(delta);
        }

        if (_wifiStatus == WL_CONNECTED) {
            MDNS.update();

            FastLEDWriter::setBrightness(_colorState.brightness);
            _ledWriter->fillAll(Black);
            for (int i = 0; i < _colorState.numOfSegments; i++) {
                _ledWriter->fill(_segments[i].start, _segments[i].end, _segments[i].color);
            }

            _defaultLedSequence->run(delta);
        }

        if (_isConnecting && _lastConnectTry + CONNECT_WIFI_TIMEOUT < millis()) {
            Serial.println("Connect timeout");

            _isConnecting = false;
            _ssid = "";
            setupAp();
        }

        _wifiStatus = currentWifiStatus;

        yield();
    }
}

void loop() {
}
