#ifndef PHTEVEN_LED_CONSTANTS_H
#define PHTEVEN_LED_CONSTANTS_H

const auto BAUD = 115200;
const auto DNS_PORT = 53;
const auto CONNECT_WIFI_INTERVAL = 2000;
const auto CONNECT_WIFI_TIMEOUT = 10000;
const auto LED_UPDATE_INTERVAL = 10;
const auto MIN_SEGMENT_OFFSET = 0;
const auto MAX_SEGMENT_OFFSET = 99;
const auto MAX_SEGMENTS = 10;

const auto EEPROM_SIZE = (MAX_SEGMENTS * 5 * 8) + (3 * 8);
const auto NUM_SEGMENTS_EEPROM_ADDRESS = 0;
const auto BRIGHTNESS_EEPROM_ADDRESS = 1;
const auto SEGMENTS_START_EEPROM_ADDRESS = BRIGHTNESS_EEPROM_ADDRESS + 1;
// 10xsegments(r,g,b,start,end) + brightness + numSegments + init-byte
const auto INIT_BYTE_ADDRESS = EEPROM_SIZE - 8;

#endif //PHTEVEN_LED_CONSTANTS_H
